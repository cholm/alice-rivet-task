Hi all, 

I've developed an standard ALICE analysis task that allows us to run
Rivet analyses (released or in development) on simulation production.
You can find the code at 

    https://gitlab.cern.ch/cholm/alice-rivet-task
	
Everyone in the group `AliRivet` should have developer access to that
repository (if not let, me know). 

The idea is to 

- Reuse the existing simulation production data for Rivet analyses,
  thus allowing us to 
  
  - Access huge amounts of data and high statistics without running
    event generators again 
  - Perform validation of analyses quickly on multiple event
    generators 
  - Use Rivet to do comparisons between data results and models 
  
- Leverage existing Grid/VAF infrastructure to do parallel processing
  of data, thus allowing us to 
  
  - Build on existing skills among collaborators 
  - Effectively, and quickly run Rivet analyses 
  
I've run tests in local mode and on PROOF(Lite) and all works as
intended.  However, for Grid deployment we _must_ have Rivet version 3
available on the Grid (we need features of Rivet version 3 - in
particular _reentrancy_ and merging of results). 

I believe that once we have Rivet version 3 available on the Grid, and
a few simple tests, we are ready to deploy this. I believe that this
will help bring down the barrier of getting collaborators to use and
develop for Grid. 

I'm not entirely sure how to best deploy this.  One way would be to
put it into to AliPhysics which would mean adding a dependency on
Rivet to AliPhysics.  Another way would be to deploy it as a separate
tool depending on AliPhysics and Rivet.  I guess we should take this
up with Dario.  

Note, non-released analyses (e.g., an analysis in development) are
supported by on-the-fly compilation of Rivet plugins. 
