#!/bin/sh
#
#  "VO_ALICE@Rivet::3.1.1-alice1-9"
#
rm -rf build.log run.log

export ALICE_TRAINS=${ALICE_TRAINS-/opt/sw/aliboard/}
s=pp
n=local
p=local
o=
b=`pwd`/data
f=
h=
a=${ALICE_TRAINS}/aliaboard

case x$1 in
    x--gdb) f="gdb --args" ; shift ;;
    *)                             ;;
esac
case x$1 in
    x--local) p=local                                 ; shift ;;
    x--lite)  p=lite  ; o=\&par\&testpar\&workers=4   ; shift ;;
    x--alien) p=alien ;
	      o=\&par\&testpar\&run=244355\&aliphysics=v5-09-44-01-1\&aliroot=v5-09-44-1\&root=v5-34-30-alice10-21\&pattern=AliESDs.root  ;
	      d=/alice/sim/2015/LHC15l1a2             ; shift ;;
    x--vaf)   p=proof ; d=/alice/sim/2015/LHC15l1a2/244355 ;
	      o=\&par\&testpar\&pattern=AliESDs.root ;
	      h=alivaf.cern.ch ; shift ;; 
    *) ;;
esac
case x$1 in
    x--pp)   s=pp   ; shift ;; # LHC16i7a Pythia6
    x--pp2)  s=pp2  ; shift ;; # LHC16i7b Phojet
    x--pp3)  s=pp3  ; shift ;; # LHC15l1a2 Pythia8
    x--pbp)  s=pbp  ; shift ;; # DPMJet 
    x--ppb)  s=ppb  ; shift ;; # DPMJet
    x--pbpb) s=pbpb ; shift ;; # Hijing
    *) ;;
esac

if test "x$d" = "x" ; then 
    d=${b}/${s}
fi
u=${p}://${h}${d}?mc${o}#esdTree

rm -rf ${p}_${s}

#	       ALICE_ROOT=/opt/sw/inst \
#	       ALICE_PHYSICS=/opt/sw/inst \
echo ${a} --class=RivetTrain --name=${p}_${s} --url=$u --ps=none $@ 
${f} ${a} --class=RivetTrain --name=${p}_${s} --url=$u \
	 --ps=none $@

