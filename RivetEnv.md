# Testing environment for Rivet 

## Get Rivet 3 to build with AliROOT on SLC-6 

Do 

    alienv enter AliPhysics/v5-09-57e-1-1

for older AliPhysics/AliROOT/ROOT suitable for SLC6.  

Then I clone 

- YODA 
- HepMC 
- Rivet 

I do this in `~/eos/RivetTest`, and then I do 

    
    git clone git@gitlab.com:hepcedar/yoda.git 
    (cd yoda && autoreconf -if)
    mkdir yoda-build 
    cd yoda-build
    ./configure --prefix=`pwd`/../inst --disable-root --disable-pyext 
    make install 
    cd ..

to build and install YODA.  For HepMC(2) I do 

    git clone ssh://git@gitlab.cern.ch:7999/hepmc/HepMC.git 
    mkdir hepmc-build
    cd hepmc-build
    cmake -DCMAKE_INSTALL_PREFIX=`pwd`/../inst -Dmomentum=GEV -Dlength=CM ../HepMC
    make install
    cd .. 

Next, we need to build Rivet.  We will use the FastJet (and Contrib) package pulled in 
by AliROOT.  However, the FastJet configuration script `fastjet-config` is broken and 
only reports linker flags to FastJet libraries itself - not to the dependency CGAL.  To 
_trick_ the build system of Rivet to work (probably also at run-time), I symlink all the
CGAL libraries to our target library directory 

    ln -s /cvmfs/alice.cern.ch/el6-x86_64/Packages/cgal/4.6.3-72/lib/libCGAL* . 

Unfortunately, we can not work around this by setting `LDFLAGS` on the `configure` 
commandline - though we still need it - as `rivet-build` does not respect such 
settingsi (see also [this issue](https://gitlab.com/hepcedar/rivet/-/issues/218)).   
Nor will it help if we set `LIBS`. 

Finally for Rivet, we do 

    git clone git@gitlab.com:hepcedar/rivet.git
    (cd rivet && autoreconf -i -f )
    (cd rivet && patch -d. -p1 < ../rivet-hepmc2.patch)
    (cd rivet && patch -d. -p1 < ../rivet-lundgen.patch)
    (cd rivet && patch -d. -p1 < ../rivet-energycorr.patch)
    (cd rivet && patch -d. -p1 < ../rivet-install.patch)
    mkdir rivet-build 
    cd rivet-build 
    ../rivet/configure --prefix=`pwd`/../inst/ --disable-pyext \
            --with-yoda=`pwd`/../inst --with-fastjet=`fastjet-config --prefix` \
            LDFLAGS=-L/cvmfs/alice.cern.ch/el6-x86_64/Packages/cgal/4.6.3-72/lib    
    make install
    cd ..
        
### Patches for Rivet 

Note the patch in line 3 above - this is to fix an issue in Rivet which applies only 
when using HepMC version 2 (see also [this issue](https://gitlab.com/hepcedar/rivet/-/issues/217)).  
Had we used HepMC version 3 we would likely not have this problem. 

The patches in line 4 and 5 above removes analyses that uses the missing `fastjet::contrib` functionality 

The content of `rivet-hepmc2.patch` 

    diff --git a/src/Tools/RivetHepMC_2.cc b/src/Tools/RivetHepMC_2.cc
    index 97b537c..9ccd99c 100644
    --- a/src/Tools/RivetHepMC_2.cc
    +++ b/src/Tools/RivetHepMC_2.cc
    @@ -160,6 +160,8 @@ namespace Rivet {
           #ifdef HEPMC_HAS_ORDERED_WEIGHTS
           // The nice way, from HepMC 2.06.11
           return ge.weights().weight_names();
    +      #elif defined(HEPMC_VERSION_CODE) && HEPMC_VERSION_CODE >= 2007000
    +      return ge.weights().keys();
           #else
           // A horrible way, before that
           map<size_t,string> idxs_keys;

The content of `rivet-lundgen.patch` 

    diff --git a/analyses/pluginATLAS/ATLAS_2020_I1790256.cc b/analyses/pluginATLAS/ATLAS_2020_I1790256.cc
    index c9486ec..26016ad 100644
    --- a/analyses/pluginATLAS/ATLAS_2020_I1790256.cc
    +++ b/analyses/pluginATLAS/ATLAS_2020_I1790256.cc
    @@ -1,4 +1,5 @@
     // -*- C++ -*-
    +#if 0 // Disabled because of missing LundGenerator
     #include "Rivet/Analysis.hh"
     #include "Rivet/Projections/FastJets.hh"
     #include "Rivet/Projections/VetoedFinalState.hh"
    @@ -160,4 +161,4 @@ namespace Rivet {
     
     
         
    -
    +#endif

The content of `rivet-energycorr.patch` 

    diff --git a/analyses/pluginCMS/CMS_2018_I1690148.cc b/analyses/pluginCMS/CMS_2018_I1690148.cc
    index 25579ce..5eff1fd 100644
    --- a/analyses/pluginCMS/CMS_2018_I1690148.cc
    +++ b/analyses/pluginCMS/CMS_2018_I1690148.cc
    @@ -1,3 +1,4 @@
    +#if 0 // Disabled because of missing fastjet::contrib::EnergyCorrelator
     #include "Rivet/Analysis.hh"
     #include "Rivet/Projections/FinalState.hh"
     #include "Rivet/Projections/FastJets.hh"
    @@ -458,3 +459,5 @@ namespace Rivet {
       DECLARE_RIVET_PLUGIN(CMS_2018_I1690148);
     
     }
    +#endif
    +

The content of `rivet-install.patch` 

    diff --git a/Makefile.am b/Makefile.am
    index 1754ef7..bb9f537 100644
    --- a/Makefile.am
    +++ b/Makefile.am
    @@ -18,11 +18,13 @@ doc:
     clean-local:
            @rm -rf a.out
     
    +if ENABLE_PYEXT
     install-data-local:
            cd doc && $(MAKE) dat json
            $(mkdir_p) $(DESTDIR)$(pkgdatadir)
            $(install_sh_DATA) doc/analists/analyses.dat $(DESTDIR)$(pkgdatadir)/
            $(install_sh_DATA) doc/analists/analyses.json $(DESTDIR)$(pkgdatadir)/
    +endif
     
     ## Upload tarballs to HepForge
     DEST = login.hepforge.org:rivet/downloads/


## Testing the set-up 

### Some data from AliEn 

I get a bit of data from the Grid 

    mkdir -p data/pp
    cd data/pp
    alien_cp alien:/alice/sim/2015/LHC15l1a2/244355/001/root_archive.zip  file:
    unzip root_archive.zip 
    cd .. 

### Train set-up 

I will use my [`ali-aboard`](https://gitlab.cern.ch/cholm/ali-aboard.git) package 
for settting up trains 

    git clone ssh://git@gitlab.cern.ch:7999/cholm/ali-aboard.git
    cd ali-aboard 
    make 
    export ALICE_TRAINS=`pwd`
    cd ..

## `AliRivetTask` 

I get the [`alice-rivet-task`](https://gitlab.cern.ch/cholm/alice-rivet-task.git) 
project

    git clone ssh://git@gitlab.cern.ch:7999/cholm/alice-rivet-task.git 

Since I've set-up `ali-aboard` and have Rivet ready too, we can try the set-up on 
the downloaded data. 


## Run 

    cd alice-rivet-task 
    ln -s ../data . 
    LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:`pwd`/../inst/lib \
    	PATH=${PATH}:`pwd`/../inst/bin \
    	./run.sh --include=`pwd`/../inst/include --include=$FASTJET/include
