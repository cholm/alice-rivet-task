# AliPhysics AliAnalysisTask to Run Rivet Analyses on Production Data

The class [`AliRivetTask`](AliRivetTask.C) defines an **ALICE**
analysis task - just like any other analysis task - that will perform
**Rivet** analyses on simulation production data.

The goal is to be able to reuse existing **ALICE** simulation
productions as input to **Rivet** analyses.  This will allow us to

- Re-use a lot of the simulated data we already have on disk for
  **Rivet** analyses.
- Utilize existing **AliEn** infrastructure to do large scale analyses 
- Fit within the analyses paradigm that most **ALICE** collaborators
  are accustomed to.
- Lower the bar for developing and contributing Rivet analyses. 

## Strategy 

The class `AliRivetTask` reads in simulated data (`Kinematics.root`)
and converts that into the **HepMC** event record format.  That event
is then passed on to **Rivet** which executes requested analyses. 

### In more details 

The task `AliRivetTask` has a single data object of the class
`AliRivetOutput`.  The class `AliRivetOutput` 

- contains a single string data member which has as content the
  _streamed_ **YODA** objects produced by a **Rivet** run. 
- can merge several objects of this kind via
  `Rivet::AnalysisHandler::mergeYodas` - which in turn executes the
  **Rivet** analyses `Rivet::Analysis::finalize` member function. 
  
This means that we can _parallelise_ data processing over many workers
(e.g., in **Proof** or **AliEn**) and get the final result back in a
standard **ROOT** file (typically `AnalysisResults.root`). 

The task `AliRivetTask` internally builds a **HepMC** event record
from the read `AliMCParticle` information.  More about the internal
workings of that later.  Furthermore, the task manages the
`Rivet::AnalysisManager` instance and pass the **HepMC** event record
to that. 

**Rivet** analyses are added to the run via the member function
`AliRivetTask::AddAnalysis` which accept the name (string) of the
analyses.

_Preloaded_ data can either be read on the client (the machine from
which the run was started) or the worker nodes (e.g., **Proof**
slaves). 

### Convert tracks to HepMC event record 

For a given `AliMCEvent` we read in the tracks.  That include tracks
corresponding to the event generator generated particles, and tracks
from **GEANT** transport.  The following describes what happens in the
member function `AliRivetTask::EncodeEvent`

For each track, starting from the bottom of the stack, we
(`AliRivetTask::GenParticle`) 

0. If we have already generated a particle from the current track, we
   do no more. 
   
1. Generate a new `HepMC3::GenParticle` (well a `std::shared_ptr` to
   it), and put a reference to it into an internal cache.

2. If the particle has any mothers, we pause this pass and do the full
   step for the mother particles.  That is, we recursively execute
   this algorithm. 

   Note, **AliROOT** seems to prune the tree for dual mothers.  That
   means we seem never to have more than one mother particle.  That
   seems to be a mistake, since we cannot identify rescatterings or
   the like. 
   
   Note, if we found that the current track was generated in a decay
   (`kPDecay`), then we modify the status of the mothers to reflect
   that (`2`). 
   
3. If we had mothers, then we get the end vertex of those.  Note, in
   principle, the vertex should be common among the mothers.  Since
   **AliROOT** only keeps one mother there's no issue here. 
   
   If the mothers didn't have end vertices and this track does not
   correspond to a beam particle (status code `4`), then we get the
   production vertex of this track and set as the end vertex of the
   mother `HepMC3::GenParticle` objects. 
   
4. If we got an vertex - either from mothers or from this tracks
   production site, we add this particle as out-going from that
   vertex. 
   
5. If the track corresponds to a beam particle (status `4`), then we
   cache it in the list of beam particles.  
   
   Note, **AliROOT** seems to remove all beam particles. 
   
6. Otherwise, if the track had no mothers, we cache the particle as an
   orphan. 
   
Once we have completed the above, we check to see whether we have any
orphans (particles with no production vertex which are not beam
particles).  If we have found beam particles - then we should have no
orphans and we issue a warning.   We then proceed to generate an
"interaction vertex" and 

- Add all orphan particles to that vertex 
- If we have no beam particles, we try to generated "fake" beam
  particles (`AliRivetTask::MakeBeams`). 
  
  To generate the fake beam particles we rely on `AliESDInfo` (via
  `AliESDEvent`) to be present.  
  
  - We query for the beam types via member functions in
    `AliESDEvent`.  If we cannot properly deduce the beams, we give
    up. 
	
  - We then get the centre-of-mass energy from the `AliESDEvent`
    object.  To make our beam particles, we need to know the
    longitudinal momentum per charge of the beams. 
	
  - To that end, we use some utility structured (`AliRivetTask::Beam`
    and `AliRivetTask::BoundBeam`). We try to calculate the
    longitudinal momentum, but if that is not possible we use a simple
    Newton-Raphson to find the longitudinal momentum needed, given the
    beam types, to get the desired centre-of-mass energy. 
   
  - Now that we know the beam particles and their longitudinal
    momentum, we can generate the corresponding `HepMC3::GenParticle`
    objects.   We add these as _incoming_ particles to the interaction
    point vertex generated above. 
	
We now have a fairly complete event tree which we pass on to
**Rivet**. 

### Encapsulating Rivet job 

The task `AliRivetTask` can be configured with which **Rivet**
analysis to perform.  The script [`AddTaskRivet.C`](AddTaskRivet.C)
together with a configuration script (example in
[`RivetConfig.C`](RivetConfig.C).  Here, one can add analyses,
preloaded data, and so on. 

At the time of the first event (`AliRivetTask::UserExec`) we set-up
the `Rivet::AnalysisHandler` and configures it according to the task
configuration (e.g., we add analyses).

Then on each event we convert the **AliROOT** simulation event
structure to the **HepMC** format.  We then pass that event on to
**Rivet** which then takes care of the rest of the steps. 

When we end the job _on the worker node_
(`AliRivetTask::FinishTaskOutput`), we perform the
`Analysis::finalize` method on all analyses. 

We then retrieve the analysis objects (AOs) from the handler, and
streams those to our internal `AliRivetOutput` object.  Note, this
assumes that we're using **Rivet** version **3** or better. 

In `AliRivetTask::Terminate`, we make sure we have stored the results
after merging over perhaps multiple partial runs.  This will also
execute the `AliRivetTask::FinishTaskOutput` member function. Note,
this assumes that we're using **Rivet** version **3** or better.

If we're working on multiple "workers" - e.g., on _Grid_ or in
**PROOF**, we have set-up merging of `AliRivetOutput` objects.  When
merging, we merge all the results together via
`Rivet::AnalysisHandler::mergeYodas`.  This only works with **Rivet**
**3** or newer.   Also note that this runs `Analysis::finalize` on all
registered analyses. 

In `AliRivetTask::Terminate`, we then take the _streamed_ **YODA** AOs
and create our final `AliRivetOutput` object, as well as a raw string
object (`TNamed` with the name `raw`).  Using either of these, we can
extract the final results of the **Rivet** analyses. 

## Custom Rivet Analyses 

The code supports custom (or not yet released) **Rivet** analyses in
all environments.  This is done by building the relevant _plugin_ on
the fly using `rivet-build` (`rivet-buildplugin` for **Rivet** version
**2**).  This is highly useful to allow large statistics runs while
developing and validating a **Rivet** analysis

## Status 

### Environments 

In general, we _must have_ **Rivet** version **3** or better.
Currently, only version **2** is available on the **Grid**.  Hence, we
cannot run the code on **Grid** at the moment.  We must work to deploy
**Rivet** version **3** on the **Grid**. 

The code works perfectly fine in local analyses, as well as via
**PROOF**.  So far, only **PROOFLite** has been tested thoroughly, but
typically this does indicate how such code would perform on all
**PROOF** cluster. 

| Environment | Works | Comments                          |
|:------------|-------|:----------------------------------|
| Local       | yes   |                                   |
| PROOF       | yes   | Only Lite tested                  |
| Grid        | no    | Rivet 3 required                  | 

### Rivet version 

Only works with **Rivet** version **3** or later.  The reason is that
we need the _reentrant_ mechanism which isn't available before. 

The code will compile with **Rivet** version **2.5.1** but the results
will be wrong or empty. 

### HepMC version 

**HepMC** version **2** and **3** are both supported. 

### Compiler 

As **Rivet** _requires_ C++11 or later, that means that so does this
code.  Thus the compiler (and **ROOT**) _must_ support C++11.  That
may mean that we need at least **ROOT** version **6**. 

### Event Generators 

The code has been used 

- Pythia6
- Pythia8
- Phojet 
- DPMJet 
- Hijing 


