// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Config/RivetConfig.hh"
#if defined(RIVET_VERSION_CODE) && RIVET_VERSION_CODE > 30000
# define USE_RIVET3
# include "Rivet/Projections/AliceCommon.hh"
#else
# include "Rivet/Projections/ChargedFinalState.hh"
#endif

namespace Rivet
{
  /** @brief Example analysis */
  class ALICE_YYYY_I1234567 : public Analysis
  {
  public:
#ifdef USE_RIVET3
    typedef ALICE::PrimaryParticles PrimProj; // Need - Rivet 3
#else
    typedef ChargedFinalState PrimProj;
#endif
    
    /** Constructor */
    ALICE_YYYY_I1234567() : Analysis("ALICE_YYYY_I1234567") {}

    /**
     * @name Analysis methods
     * @{
     */
    /** Book histograms and initialise projections before the run */
    void init()
    {
      // Initialise and register projections
      declare(PrimProj(Cuts::abseta < 5 && Cuts::abscharge3 > 0),"APRIM");
           
#ifdef USE_RIVET3
      book(_h, 1, 1, 1);
#else
      _h = bookHisto1D(1,1,1);
#endif
    }
    /** Perform the per-event analysis */
    void analyze(const Event& event)
    {
      for (auto p : apply<PrimProj>(event,"APRIM").particles())
	_h->fill(p.eta());
    }
    /** Normalise histograms etc., after the run */
    void finalize()
    {
      normalize(_h);
    }
    /* @} */

    /**
     * @name Histograms
     * @{
     */
    Histo1DPtr _h;
    /* @} */
  };
#ifdef USE_RIVET3
  RIVET_DECLARE_PLUGIN(ALICE_YYYY_I1234567);
#else
  DECLARE_RIVET_PLUGIN(ALICE_YYYY_I1234567);
#endif
}

//
// EOF
//
